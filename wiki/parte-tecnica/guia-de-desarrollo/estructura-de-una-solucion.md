# Estructura de una solución de Visual Studio

Una solución de una aplicación que usa LoB debe organizarse por carpetas de solucion. Las carpetas de solución que albergan proyectos del núcleo deben situarse arriba y conforme vamos bajando en las carpetas estas deben ser más especificas de la aplicación, de forma que las primeras deben ser lás más genéricas y las últimas las más especificas. Por último se encontrará la aplicación ejecutable en sí, que no se alberga en ninguna carpeta de solución de forma que siempre está a la vista.
en cambio la primera carpeta de soluciones que debe aparecer es la carpeta de `documentación`, en dicha carpeta se referencias los archivos que se adjuntan a la aplicación que aporten la documentación necesaria que se crea oportuna.  
Para conseguir este orden, y debido a que Visual Studio ordena las carpetas por nombre, se harán uso de los prefijos que sean necesarios para imponer este orden, siempre y cuando no dificulten la lectura del nombre de la carpeta. Por ejemplo al ser la carpeta de documentación se podrá llamar `_Documentation`, el simbolo `_` nos asegura que aparezca en primer lugar, si este simbolo no fuese suficiente por lo que sea, se busca otro.

Un ejemplo de solución sería el siguiente:

* Solución 'CalzadosGranVia' 
  * _Documentación (carpeta de documentación)
  * _IoC (carpeta con la extensión de StructureMap para el IoC)
  * _LoB Core (carpeta con proyectos del núcleo)
  * _LoB Warehouse (carpeta con proyectos de código relacionado con el almacén)
  * CGV Application (Carpeta con proyectos de código para la aplicación 'CalzadosGranVia)
  * CGVWeb (Proyecto web ejecutable con la aplicación final)
  
Podemos observar que la primera carpeta es `_Documentación`, luego vamos metiendo caretas en orden de especialización, terminando con la carpeta con el código especifico para la aplicación (`CGV Application`) y por último el proyecto web ejecutable, que está fuera de toda carpeta y siempre es visible cuando collapsamos el arbol de la solución.
 