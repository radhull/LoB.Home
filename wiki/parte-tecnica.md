# Line Of Business Library

Line Of Business (LoB) es una librería de código .NET para construir aplicaciones de gestión. LoB intenta implantar la *filosofía* [GTD (Get the Things Done)](https://es.wikipedia.org/wiki/Getting_Things_Done), por ello LoB antepone la agilidad en el desarrollo al seguimiento estricto de patrones estándar, dando la posibilidad de realizar nuevas características creando el mínimo de código y archivos nuevos.  
Para ello se usa una arquitectura similar a Desarrollo Dirigido por el Dominio (Domain driven develpment) [DDD](https://es.wikipedia.org/wiki/Dise%C3%B1o_guiado_por_el_dominio) pero mucho menos rígida, donde en los casos que no sea necesario las capas intermedias no existen.

LoB también tiene en cuenta los principios [SOLID](https://es.wikipedia.org/wiki/SOLID_(object-oriented_design))

Tecnologías y estrategias de desarrollo usadas:

## Herramientas
* Visual Studio 2013/2015+ .NET Framework 4.6+
* NodeJs, Gulp para el código del cliente web.

## Base
* Inversión de control e inyección de dependencias. Soporta cualquier contenedor de dependencias aunque LoB incorpora el assembly **LoB.IoC.StructureMap** con lo necesario para arrancar usando [StructureMap 4.x](http://structuremap.github.io/)
* Acceso a base de datos a usando [Entity Framework 6](https://entityframework.codeplex.com/).

## Web
* **ASP.NET 4.5 y MVC 5** La parte web de LoB está realizada usando una implementación especial de ASP.NET MVC. Con el fin de evitar tener que desarrollar controladores y view models para cada pantalla o entidad.  
LoB ya dispone de unos controladores base que realizan la comunicación con los servicios de aplicación, así como el acceso a las vistas específicas.
* **Angular 1.5** LoB dispone de código base para el cliente web en **LoB.Web.Client**, se trata de una serie de clases así como componentes, directivas y servicios Angular.

## Comunicaciones
* **ASP.NET SignalR** LoB usa [SignalR](http://www.asp.net/signalr) para la comunicación en tiempo real entre los distintos módulos de la aplicación.

# Dependencias asumidas

LoB asume la dependencia de:

1. [Entity Framework 6](https://entityframework.codeplex.com/)
2. [Json.Net (NewtonSoft.Json)](http://www.newtonsoft.com/json)

Lo cual quiere decir que en los proyectos del núcleo existen esas referencias y que se renuncia a trabajar sin dichas referencias, otra cosa es que, en cada caso, se estén usando. 
